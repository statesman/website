---
path: /events/peermeet20
date: "2020-01-16"
author: "tupio"
title: "Peer Meet"
cover: "./poster2.jpeg"
name: "Denil C V"
---

![Poster](./poster2.jpeg)

FOSS Cell, NSSCE is conducting a peer meet on the topic "Relevance of FOSS and its use in engineering fields". The event hopes to encompass discussion on applications and wide opportunities of free & open-source software in various engineering sectors.
Heartly welcome you all to the same.

## Event Details

Date: 16/01/2020
Time: 4.15 PM
Venue: DLH (Alumni block)
